package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import domain.Camera;
import domain.Cor;
import domain.Luz;
import domain.Objeto;
import domain.Ponto;

public class UtilLeitura {

	public static Camera lerCamera(String path) throws FileNotFoundException{
		Scanner lerArquivo = new Scanner(new File(path));	
		Ponto C, N, V, U;
		double d, hx, hy;
		String[] arq = new String[12];

		// Leitura
		for(int i = 0; i < 12; i++ ){
			arq[i] = lerArquivo.next();
		}
		lerArquivo.close();
		C = new Ponto(Double.valueOf(arq[0]), Double.valueOf(arq[1]), Double.valueOf(arq[2]));
		N = new Ponto(Double.valueOf(arq[3]), Double.valueOf(arq[4]), Double.valueOf(arq[5]));
		V = new Ponto(Double.valueOf(arq[6]), Double.valueOf(arq[7]), Double.valueOf(arq[8]));
		d = Double.valueOf(arq[9]);
		hx = Double.valueOf(arq[10]);
		hy = Double.valueOf(arq[11]);

		// Ortogonaliza V:
		double coef = (UtilContas.produtoInterno(V, N) / UtilContas.produtoInterno(N, N));

		V.atualizarCoordenadas(V.x - (coef * N.x), V.y - (coef * N.y), V.z - (coef * N.z));

		// normaliza N e V:
		UtilContas.normalizar(V);
		UtilContas.normalizar(N);

		// calcula U:
		U = UtilContas.produtoVetorial(N, V);

		Camera camera = new Camera(C, V, N, U, d, hx, hy);

		return camera;
	}

	public static Objeto lerObjeto(Camera camera, String path, int resX, int resY) throws FileNotFoundException{

		Scanner lerArquivo = new Scanner(new File(path));
		
		// Leitura
		String qtdPontosStr = lerArquivo.next();
		String qtdTriangulosStr = lerArquivo.next();
		double xMin = 0, xMax = 0, yMin = 0, yMax = 0, zMin = 0, zMax = 0;

		int qtdPontos = Integer.valueOf(qtdPontosStr);
		int qtdTriangulos = Integer.valueOf(qtdTriangulosStr);
		String x, y, z;

		Ponto[] pontos = new Ponto[qtdPontos];
		Ponto[] pontosVisao = new Ponto[qtdPontos];
		int[][] triangulos = new int[qtdTriangulos][3];
		
		for( int i = 0; i < qtdPontos; i++ ){
			x = lerArquivo.next();
			y = lerArquivo.next();
			z = lerArquivo.next();
			pontos[i] = new Ponto(Double.valueOf(x), Double.valueOf(y), Double.valueOf(z), i);

			pontos[i].subtracao(camera.C);

			double xVisao = (pontos[i].x * camera.U.x) + (pontos[i].y * camera.U.y) + (pontos[i].z * camera.U.z);
			double yVisao = (pontos[i].x * camera.V.x) + (pontos[i].y * camera.V.y) + (pontos[i].z * camera.V.z);
			double zVisao = (pontos[i].x * camera.N.x) + (pontos[i].y * camera.N.y) + (pontos[i].z * camera.N.z);

			pontosVisao[i] = new Ponto(xVisao, yVisao, zVisao, i);
				
			pontos[i].atualizarCoordenadas((camera.d / camera.hx) * ( xVisao / zVisao ), (camera.d / camera.hy) * ( yVisao / zVisao ), 0);
			pontos[i].atualizarCoordenadas((int) (( pontos[i].x + 1 ) * ( resX - 1 ) / 2), (int) (( 1 - pontos[i].y ) * ( resY - 1 ) / 2), 0);
			
			if ( pontosVisao[i].x > xMax ){
				xMax = pontosVisao[i].x;
			}
			if ( pontosVisao[i].y > yMax ){
				yMax = pontosVisao[i].y;
			}
			if ( pontosVisao[i].z > zMax ){
				zMax = pontosVisao[i].z;
			}
			if ( pontosVisao[i].x < xMin ){
				xMin = pontosVisao[i].x;
			}
			if ( pontosVisao[i].y < yMin ){
				yMin = pontosVisao[i].y;
			}
			if ( pontosVisao[i].z < zMin ){
				zMin = pontosVisao[i].z;
			}
			
		}

		for( int i = 0; i < qtdTriangulos; i++ ){
			x = lerArquivo.next();
			y = lerArquivo.next();
			z = lerArquivo.next();
			triangulos[i][0] = Integer.valueOf(x) - 1;
			triangulos[i][1] = Integer.valueOf(y) - 1;
			triangulos[i][2] = Integer.valueOf(z) - 1;
		}

		lerArquivo.close();

		Ponto v1 = new Ponto(0, 0, 0), v2 = new Ponto(1, 1, 1);
		Ponto[] normaisTriangulos = new Ponto[qtdTriangulos];
		Ponto[] normaisVertices = new Ponto[qtdPontos];

		for (int i = 0; i < qtdPontos; i++) {
			normaisVertices[i] = new Ponto(0.0, 0.0, 0.0);
		}

		for (int i = 0; i < qtdTriangulos; i++) {
			Ponto p1 = pontosVisao[triangulos[i][0]];
			Ponto p2 = pontosVisao[triangulos[i][1]];
			Ponto p3 = pontosVisao[triangulos[i][2]];

			v1.x = p2.x - p1.x;
			v1.y = p2.y - p1.y;
			v1.z = p2.z - p1.z;
			v2.x = p3.x - p1.x;
			v2.y = p3.y - p1.y;
			v2.z = p3.z - p1.z;

			normaisTriangulos[i] = UtilContas.produtoVetorial(v1, v2);
			UtilContas.normalizar(normaisTriangulos[i]);

			normaisVertices[triangulos[i][0]].x += normaisTriangulos[i].x;
			normaisVertices[triangulos[i][0]].y += normaisTriangulos[i].y;
			normaisVertices[triangulos[i][0]].z += normaisTriangulos[i].z;

			normaisVertices[triangulos[i][1]].x += normaisTriangulos[i].x;
			normaisVertices[triangulos[i][1]].y += normaisTriangulos[i].y;
			normaisVertices[triangulos[i][1]].z += normaisTriangulos[i].z;

			normaisVertices[triangulos[i][2]].x += normaisTriangulos[i].x;
			normaisVertices[triangulos[i][2]].y += normaisTriangulos[i].y;
			normaisVertices[triangulos[i][2]].z += normaisTriangulos[i].z;

		}
		for (int i = 0; i < qtdPontos; i++) {
			UtilContas.normalizar(normaisVertices[i]);
		}

		Objeto objeto = new Objeto(pontosVisao, pontos, normaisTriangulos, normaisVertices, triangulos, xMin, xMax, yMin, yMax, zMin, zMax);
		return objeto;
	}

	public static Luz lerLuz(Camera camera) throws FileNotFoundException{
		Scanner lerArquivo = new Scanner(new File("Entradas\\iluminacao.txt"));	
		Ponto pl, ia, od;
		double ka, kd, ks, n;
		Cor il;

		String[] arq = new String[16];
		for(int i = 0; i < 16; i++ ){
			arq[i] = lerArquivo.next();
		}
		lerArquivo.close();
		pl = new Ponto(Double.valueOf(arq[0]), Double.valueOf(arq[1]), Double.valueOf(arq[2]));
		ia = new Ponto(Double.valueOf(arq[4]), Double.valueOf(arq[5]), Double.valueOf(arq[6]));
		od = new Ponto(Double.valueOf(arq[8]), Double.valueOf(arq[9]), Double.valueOf(arq[10]));
		ka = Double.valueOf(arq[3]);
		kd = Double.valueOf(arq[7]);
		ks = Double.valueOf(arq[11]);
		il = new Cor(Double.valueOf(arq[12]), Double.valueOf(arq[13]), Double.valueOf(arq[14]));
		n = Double.valueOf(arq[15]);
		
		pl.subtracao(camera.C);

		double xVisao = (pl.x * camera.U.x) + (pl.y * camera.U.y) + (pl.z * camera.U.z);
		double yVisao = (pl.x * camera.V.x) + (pl.y * camera.V.y) + (pl.z * camera.V.z);
		double zVisao = (pl.x * camera.N.x) + (pl.y * camera.N.y) + (pl.z * camera.N.z);

		pl.atualizarCoordenadas(xVisao, yVisao, zVisao);
		
		Luz luz = new Luz(pl, ia, od, ka, kd, ks, n, il);
		return luz;
	}
}
