package util;

import domain.Cor;
import domain.Ponto;

public class UtilContas {

	public static double[] pontoToArray(Ponto a){
		double[] retorno = { a.x, a.y, a.z };
		return retorno;
	}

	public static double produtoInterno(Ponto a, Ponto b) {
		return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
	}
		
	public static void normalizar(Ponto a){
		double norma = Math.sqrt(UtilContas.produtoInterno(a, a));
		a.x /= norma;
		a.y /= norma;
		a.z /= norma;
	}	
	
	public static Ponto subtracao(Ponto a, Ponto b){
		return new Ponto(a.x - b.x, a.y - b.y, a.z - b.z);
	}
	
	public static Ponto multiplicaoPontoEscalar(double a, Ponto b){
		return new Ponto(a * b.x, a * b.y, a * b.z);
	}
	
	public static Ponto produtoVetorial(Ponto u, Ponto v) {
		double i, j, k;
		i = (u.y*v.z) - (u.z*v.y);
		j = (u.z*v.x) - (u.x*v.z);
		k = (u.x*v.y) - (u.y*v.x);

		return new Ponto(i, j, k);
	}

	public static double[] produtoCorPonto(Cor a, Ponto b) {
		double[] retorno = { a.red * b.x, a.green * b.y, a.blue * b.z };
		return retorno;
	}
	
	public static double[] produtoCorEscalar(Cor a, double b) {
		double[] retorno = { a.red * b, a.green * b, a.blue * b };
		return retorno;
	}

	public static double[] calcularCoordenadasBaricentricas(Ponto p, Ponto p1, Ponto p2, Ponto p3) {
		double x = p.x, y = p.y, x1 = p1.x, x2 = p2.x, x3 = p3.x, y1 = p1.y, y2 = p2.y, y3 = p3.y;
		double alfa, beta, gama;
		double denom = ((x1 - x3) * (y2 - y3) - (y1 - y3) * (x2 - x3)); 
		alfa = ((y2 - y3) * (x - x3) - ((x3 - x2) * (y3 - y))) / denom;
		beta = (((y3 - y1) * (x - x3)) - ((x1 - x3) * (y3 - y))) / denom;
		gama = 1.0 - alfa - beta;

		double[] retorno = { alfa, beta, gama };
		return retorno;
	}
	
	public static void rotateX(Ponto a, double graus){
		graus = Math.toRadians(graus);
		double y,z;
		
		y = a.y * Math.cos(graus) + a.z * -Math.sin(graus);
		z = a.y * Math.sin(graus) + a.z * Math.cos(graus);
		a.y = y;
		a.z = z;
	}
	
	public static void rotateY(Ponto a, double graus){
		graus = Math.toRadians(graus);
		double x,z;
		
		x = a.x * Math.cos(graus) + a.z * -Math.sin(graus);
		z = a.x * Math.sin(graus) + a.z * Math.cos(graus);
		a.x = x;
		a.z = z;
	}
	
	public static void rotateZ(Ponto a, double graus){
		graus = Math.toRadians(graus);
		double x,y;
		
		x = a.x * Math.cos(graus) + a.y * -Math.sin(graus);
		y = a.x * Math.sin(graus) + a.y * Math.cos(graus);
		a.x = x;
		a.y = y;
	}
}
