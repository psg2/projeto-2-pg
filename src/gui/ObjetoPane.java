package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import domain.Camera;
import domain.Luz;
import domain.Objeto;
import domain.Ponto;
import util.UtilContas;

@SuppressWarnings("serial")
public class ObjetoPane extends JPanel {

	Camera camera;
	Objeto objeto;
	Luz luz;
	double zBuffer[][];
	Graphics2D g2d;
	BufferedImage[] texturas;
	int qtdTexturas;
	boolean texturaOn;
	int resX;
	int resY;

	public ObjetoPane(Camera camera, Objeto objeto, Luz luz, int resX, int resY) {
		this.camera = camera;
		this.objeto = objeto;
		this.luz = luz;
		texturaOn = false;
		this.resX = resX;
		this.resY = resY;
	}

	public ObjetoPane(Camera camera, Objeto objeto, Luz luz, String entradaTextura, int resX, int resY) throws IOException {
		this.camera = camera;
		this.objeto = objeto;
		this.luz = luz;
		String[] texturasStr = entradaTextura.split("/");
		this.qtdTexturas = texturasStr.length;
		this.texturas = new BufferedImage[qtdTexturas];
		for( int i = 0; i < this.qtdTexturas; i++ ){
			texturas[i] = ImageIO.read(new File("Entradas\\Texturas\\" + texturasStr[i] + ".jpg"));
		}	
		this.texturaOn = true;
		this.resX = resX;
		this.resY = resY;
	}

	private void scanline(Ponto P1, Ponto P2, Ponto P3, double y, double xMin, double xMax ) {
		Ponto P;
		for (int x = (int)Math.ceil(xMin); x <= Math.floor(xMax); x++) {
			double[] temp = UtilContas.calcularCoordenadasBaricentricas(new Ponto(x, y, 0), P1, P2, P3);
			double alfa = temp[0];
			double beta = temp[1];
			double gama = temp[2];

			if ( gama >= 0 && beta >= 0 && alfa >= 0 )  {
				Ponto P1Visao, P2Visao, P3Visao;
				P1Visao = objeto.pontosVisao[P1.indice];
				P2Visao = objeto.pontosVisao[P2.indice];
				P3Visao = objeto.pontosVisao[P3.indice];

				double xP = alfa*P1Visao.x + beta*P2Visao.x + gama*P3Visao.x;
				double yP =  alfa*P1Visao.y + beta*P2Visao.y + gama*P3Visao.y;
				double zP = alfa*P1Visao.z + beta*P2Visao.z + gama*P3Visao.z;

				P = new Ponto(xP, yP, zP);

				if (x >= 0 && y >= 0 && x < zBuffer.length && y < zBuffer[0].length) {
					if (P.z < zBuffer[(int)x][(int)y] && P.z > 0) {
						zBuffer[(int) x][(int) y] = P.z;
						Ponto N1 = objeto.normaisVertices[P1.indice];
						Ponto N2 = objeto.normaisVertices[P2.indice];
						Ponto N3 = objeto.normaisVertices[P3.indice];

						double xN = alfa*N1.x + beta*N2.x + gama*N3.x;
						double yN =  alfa*N1.y + beta*N2.y + gama*N3.y;
						double zN = alfa*N1.z + beta*N2.z + gama*N3.z;

						Ponto V = new Ponto( (-1)*P.x, (-1)*P.y, (-1)*P.z);

						Ponto N = new Ponto( xN, yN, zN );

						UtilContas.normalizar(V);

						if (UtilContas.produtoInterno(V, N) < 0) {
							N.inverter();
						}

						double[] cor = getCor(P, N);
						g2d.setColor(new Color((int)cor[0], (int)cor[1], (int)cor[2]));
						g2d.drawLine((int) x, (int) y, (int) x, (int) y);
					}
				}
			}
		}
	}

	private void desenhar(Graphics g) {
		g2d = (Graphics2D) g;
		zBuffer = new double[resX + 1][resY + 1];
		for (int i = 0; i < resX; i++) {
			for (int j = 0; j < resY; j++) {
				zBuffer[i][j] = Double.MAX_VALUE;
			}
		}
		int limite = objeto.triangulos.length;

		for (int i = 0; i < limite; i++) {
			Ponto P1, P2, P3;
			double xMin, xMax, yMax, yMin;

			P1 = objeto.pontos2D[objeto.triangulos[i][0]];
			P2 = objeto.pontos2D[objeto.triangulos[i][1]];
			P3 = objeto.pontos2D[objeto.triangulos[i][2]];

			yMin = Math.min(P3.y, Math.min(P1.y, P2.y) );
			yMax = Math.max(P3.y, Math.max(P1.y, P2.y) );

			xMin = Math.min(P3.x, Math.min(P1.x, P2.x) );
			xMax = Math.max(P3.x, Math.max(P1.x, P2.x) );

			for (int y = (int)Math.ceil(yMin); y <= Math.floor(yMax); y++) {
				scanline(P1, P2, P3, y, xMin, xMax);
			}
		}

	}

	private double[] getCor(Ponto P, Ponto N) {
		Ponto vetorL = UtilContas.subtracao(luz.pl, P);

		UtilContas.normalizar(vetorL);

		double produtoInternoLN = Math.max( 0, UtilContas.produtoInterno(vetorL, N) );

		Ponto od = null;
		int height = 0, width = 0, fx = 0, fy = 0, fz1 = 0, fz2 = 0;
		double fzD = 0;
		if ( texturaOn ){
			height = texturas[0].getHeight();
			width = texturas[0].getWidth();

			if ( qtdTexturas == 1 ){
				fx = (int) ( ( ( P.x - objeto.xMin ) / ( objeto.xMax - objeto.xMin + 1 ) ) * height );
				fy = (int) ( ( ( P.y - objeto.yMin ) / ( objeto.yMax - objeto.yMin + 1 ) ) * width  );

				int rgb = texturas[0].getRGB(fx, fy);
				Color color = new Color(rgb);

				od = new Ponto(color.getRed() / 255.0, color.getGreen() / 255.0, color.getBlue() / 255.0);
			} else {
				fzD = ( ( ( P.y - objeto.yMin ) / ( objeto.yMax - objeto.yMin ) ) * ( qtdTexturas -1 )  );
				fz1 = (int) Math.floor(fzD);
				fz2 = (int) Math.ceil(fzD);

				double fator1 = Math.ceil(fzD) - fzD;
				double fator2 = fzD - Math.floor(fzD);

				fx = (int) ( ( ( P.x - objeto.xMin ) / ( objeto.xMax - objeto.xMin ) ) * ( height - 1 ) );
				fy = (int) ( ( ( P.z - objeto.zMin ) / ( objeto.zMax - objeto.zMin ) ) * ( width - 1 ) );

				int rgb1 = texturas[fz1].getRGB(fx, fy);
				Color color1 = new Color(rgb1);
				int rgb2 = texturas[fz2].getRGB(fx, fy);
				Color color2 = new Color(rgb2);
				double red1 = ( color1.getRed() * fator1 ) / 255;
				double red2 = ( color2.getRed() * fator2 ) / 255;
				double green1 = ( color1.getGreen() * fator1 ) / 255;
				double green2 = ( color2.getGreen() * fator2 ) / 255;
				double blue1 = ( color1.getBlue() * fator1 ) / 255;
				double blue2 = ( color2.getBlue() * fator2 ) / 255;

				od = new Ponto(red1 + red2, green1 + green2, blue1 + blue2);
			}
		} else {
			od = luz.od;
		}
		
		double[] id = new double[3];
		double[] is = new double[3];
		double[] retorno = new double[3];

		double[] produtoIlOd = UtilContas.produtoCorPonto(luz.il, od);		
		id[0] = produtoIlOd[0] * (luz.kd * produtoInternoLN);
		id[1] = produtoIlOd[1] * (luz.kd * produtoInternoLN);
		id[2] = produtoIlOd[2] * (luz.kd * produtoInternoLN);

		double coef = UtilContas.produtoInterno(vetorL, N);

		double x = ( coef * N.x * 2 ) - vetorL.x;
		double y = ( coef * N.y * 2 ) - vetorL.y;
		double z = ( coef * N.z * 2 ) - vetorL.z;
	
		Ponto vetorR = new Ponto(x, y, z);
		
		UtilContas.normalizar(vetorR);

		Ponto V = new Ponto( (-1)*P.x, (-1)*P.y, (-1)*P.z);

		UtilContas.normalizar(V);

		double rug = Math.max( 0, UtilContas.produtoInterno(vetorR, V) );
		rug = Math.pow(rug,luz.n);
		rug *= luz.ks;

		Ponto iaPonto = UtilContas.multiplicaoPontoEscalar(luz.ka, luz.ia);
		is = UtilContas.produtoCorEscalar(luz.il, rug);
		double[] iaArray = iaPonto.toArray();
		
		retorno[0] = Math.min(is[0] + id[0] + iaArray[0], 255);
		retorno[1] = Math.min(is[1] + id[1] + iaArray[1], 255);
		retorno[2] = Math.min(is[2] + id[2] + iaArray[2], 255);

		return retorno;
	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		desenhar(g);
	}

}