package gui;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import domain.Camera;
import domain.Luz;
import domain.Objeto;
import util.UtilContas;
import util.UtilLeitura;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class Main extends JFrame {

	private JPanel contentPane;
	private JTextField textCamera;
	private JTextField textObjeto;
	private JPanel visualPane;
	private JButton btnVisualizar;
	private JButton btnSair;
	private JTextField textTextura;
	private Camera camera;
	private Camera cameraAuxiliar;
	private Objeto objeto;
	private Objeto objetoAuxiliar;
	private Luz luz;
	private Luz luzAuxiliar;
	private int resX = 800;
	private int resY = 600;
	private String entradaTextura;
	private String entradaObjeto;
	private double graus = 5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, fall back to cross-platform
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (Exception ex) {
				// not worth my time
			}
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
					frame.setSize(900, 700);
					frame.setTitle("PG - Textura S�lida ( psg2, tfb )"); 
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 643, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		contentPane.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			@Override
			public void keyReleased(KeyEvent e) {				
			}
			@Override
			public void keyPressed(KeyEvent e) {
				try {
					if ( e.getKeyCode() == KeyEvent.VK_EQUALS ){
						zoom(1);
					} else if ( e.getKeyCode() == KeyEvent.VK_MINUS ){
						zoom(0);
					} else if ( e.getKeyCode() == KeyEvent.VK_UP ){
						rotateX(1);
					} else if ( e.getKeyCode() == KeyEvent.VK_DOWN ){
						rotateX(0);
					} else if ( e.getKeyCode() == KeyEvent.VK_RIGHT ){
						rotateY(1);
					} else if ( e.getKeyCode() == KeyEvent.VK_LEFT ){
						rotateY(0);
					} else if ( e.getKeyCode() == KeyEvent.VK_PAGE_UP ){
						rotateZ(1);
					} else if ( e.getKeyCode() == KeyEvent.VK_PAGE_DOWN ){
						rotateZ(0);
					}
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});

		JLabel lblCamera = new JLabel("C\u00E2mera : ");
		lblCamera.setBounds(10, 11, 68, 14);
		contentPane.add(lblCamera);

		textCamera = new JTextField();
		textCamera.setBounds(68, 8, 112, 25);
		contentPane.add(textCamera);
		textCamera.setColumns(10);

		JLabel lblObjeto = new JLabel("Objeto :");
		lblObjeto.setBounds(192, 11, 46, 14);
		contentPane.add(lblObjeto);

		textObjeto = new JTextField();
		textObjeto.setBounds(250, 8, 122, 25);
		contentPane.add(textObjeto);
		textObjeto.setColumns(10);

		btnVisualizar = new JButton("Visualizar");
		btnVisualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					printObject();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnVisualizar.setBounds(620, 8, 129, 25);
		contentPane.add(btnVisualizar);

		btnSair = new JButton("Resetar");
		btnSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					reset();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnSair.setBounds(750, 8, 129, 25);
		contentPane.add(btnSair);

		visualPane = new JPanel();
		visualPane.setBounds(6, 58, 800, 583);
		visualPane.setSize(800, 600);
		contentPane.add(visualPane);

		JLabel lblTextura = new JLabel("Textura :");
		lblTextura.setBounds(384, 10, 55, 16);
		contentPane.add(lblTextura);

		textTextura = new JTextField();
		textTextura.setBounds(451, 8, 112, 25);
		contentPane.add(textTextura);
		textTextura.setColumns(10);
	}

	void printObject() throws NumberFormatException, IOException{

		String entradaCamera = "Entradas\\Cameras\\" + textCamera.getText() + ".cfg";
		entradaObjeto = "Entradas\\Objetos\\" + textObjeto.getText() + ".byu";
		entradaTextura = textTextura.getText();

		int resX = 800;
		int resY = 600;
		camera = UtilLeitura.lerCamera(entradaCamera);
		objeto = UtilLeitura.lerObjeto(camera, entradaObjeto, resX, resY);
		luz = UtilLeitura.lerLuz(camera);
		cameraAuxiliar = camera.copy();

		ObjetoPane teste = null;

		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(camera, objeto, luz, resX, resY);		
		} else {
			teste = new ObjetoPane(camera, objeto, luz, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		btnVisualizar.setFocusable(false);
		textCamera.setFocusable(false);
		textTextura.setFocusable(false);
		textObjeto.setFocusable(false);
		contentPane.setFocusable(true);
		btnSair.setFocusable(false);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}

	void reset() throws IOException{
		textCamera.setFocusable(true);
		textTextura.setFocusable(true);
		textObjeto.setFocusable(true);	
		cameraAuxiliar = camera.copy();

		ObjetoPane teste = null;
		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(camera, objeto, luz, resX, resY);		
		} else {
			teste = new ObjetoPane(camera, objeto, luz, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}

	void zoom(int type) throws IOException{
		if ( type == 1 ){
			cameraAuxiliar.d = cameraAuxiliar.d + 0.5;	
		} else {
			cameraAuxiliar.d = cameraAuxiliar.d - 0.5;	
		}

		objetoAuxiliar = UtilLeitura.lerObjeto(cameraAuxiliar, entradaObjeto, resX, resY);
		luzAuxiliar = UtilLeitura.lerLuz(cameraAuxiliar);

		ObjetoPane teste = null;
		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, resX, resY);		
		} else {
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}
	
	void rotateX(int type) throws IOException{
		if ( type == 1 ){
			UtilContas.rotateX(cameraAuxiliar.C, graus);
			UtilContas.rotateX(cameraAuxiliar.N, graus);
			UtilContas.rotateX(cameraAuxiliar.V, graus);
		} else {
			UtilContas.rotateX(cameraAuxiliar.C, -graus);
			UtilContas.rotateX(cameraAuxiliar.N, -graus);
			UtilContas.rotateX(cameraAuxiliar.V, -graus);
		}

		objetoAuxiliar = UtilLeitura.lerObjeto(cameraAuxiliar, entradaObjeto, resX, resY);
		luzAuxiliar = UtilLeitura.lerLuz(cameraAuxiliar);

		ObjetoPane teste = null;
		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, resX, resY);		
		} else {
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}
	
	void rotateY(int type) throws IOException{
		if ( type == 1 ){
			UtilContas.rotateY(cameraAuxiliar.C, graus);
			UtilContas.rotateY(cameraAuxiliar.N, graus);
			UtilContas.rotateY(cameraAuxiliar.V, graus);
		} else {
			UtilContas.rotateY(cameraAuxiliar.C, -graus);
			UtilContas.rotateY(cameraAuxiliar.N, -graus);
			UtilContas.rotateY(cameraAuxiliar.V, -graus);
		}

		objetoAuxiliar = UtilLeitura.lerObjeto(cameraAuxiliar, entradaObjeto, resX, resY);
		luzAuxiliar = UtilLeitura.lerLuz(cameraAuxiliar);

		ObjetoPane teste = null;
		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, resX, resY);		
		} else {
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}
	
	void rotateZ(int type) throws IOException{
		if ( type == 1 ){
			UtilContas.rotateZ(cameraAuxiliar.C, graus);
			UtilContas.rotateZ(cameraAuxiliar.N, graus);
			UtilContas.rotateZ(cameraAuxiliar.V, graus);
		} else {
			UtilContas.rotateZ(cameraAuxiliar.C, -graus);
			UtilContas.rotateZ(cameraAuxiliar.N, -graus);
			UtilContas.rotateZ(cameraAuxiliar.V, -graus);
		}

		objetoAuxiliar = UtilLeitura.lerObjeto(cameraAuxiliar, entradaObjeto, resX, resY);
		luzAuxiliar = UtilLeitura.lerLuz(cameraAuxiliar);

		ObjetoPane teste = null;
		if ( textTextura.getText().isEmpty() ){
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, resX, resY);		
		} else {
			teste = new ObjetoPane(cameraAuxiliar, objetoAuxiliar, luzAuxiliar, entradaTextura, resX, resY);		
		}
		teste.setBounds(0, 0, resX, resY);

		visualPane.removeAll();
		visualPane.add(teste);
		contentPane.repaint();
	}
}
