package domain;

public class Ponto {
	public double x, y, z;
	public int indice;

	public Ponto(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Ponto(double x, double y, double z, int indice) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.indice = indice;
	}

	public String toString(){
		return "(" + this.x + ", " + this.y + ", " + this.z + ")";
	}
	
	public Ponto copy(){
		return new Ponto(this.x, this.y, this.z);
	}
		
	public void atualizarCoordenadas(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public void subtracao(Ponto b){
		this.x -= b.x;
		this.y -= b.y;
		this.z -= b.z;
	}
	
	public void inverter(){
		this.x = (-1) * this.x;
		this.y = (-1) * this.y;
		this.z = (-1) * this.z;
	}
	
	public double[] toArray(){
		double[] retorno = { this.x, this.y, this.z };
		return retorno;
	}
}
