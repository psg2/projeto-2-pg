package domain;

public class Luz {
	public Ponto pl, ia, od;
	public double ka, kd, ks, n;
	public Cor il;

	public Luz(Ponto pl, Ponto ia, Ponto od, double ka, double kd, double ks, double n, Cor il ) {
		this.pl = pl;
		this.ia = ia;
		this.od = od;
		this.ka = ka;
		this.kd = kd;
		this.ks = ks;
		this.n = n;
		this.il = il;
		
	}
}
	