package domain;

public class Cor {
	public double red, green, blue;
	
	public Cor(double red, double green, double blue){
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public Cor copy(){
		return new Cor(this.red, this.green, this.blue);
	}
}
