package domain;

public class Objeto {
	public Ponto pontosVisao[], pontos2D[], normaisTriangulos[], normaisVertices[];
	public int triangulos[][];
	public double xMin, xMax, yMin, yMax, zMin, zMax;
	
	public Objeto(Ponto[] pontosVisao, Ponto[] pontos2D, Ponto[] normaisTriangulos, Ponto[] normaisVertices, int[][] triangulos) {
		this.pontosVisao = pontosVisao;
		this.pontos2D = pontos2D;
		this.normaisTriangulos = normaisTriangulos;
		this.normaisVertices = normaisVertices;
		this.triangulos = triangulos;
	}

	public Objeto(Ponto[] pontosVisao, Ponto[] pontos, Ponto[] normaisTriangulos, Ponto[] normaisVertices, int[][] triangulos, double xMin, double xMax, double yMin, double yMax, double zMin, double zMax) {
		this.pontosVisao = pontosVisao;
		this.pontos2D = pontos;
		this.normaisTriangulos = normaisTriangulos;
		this.normaisVertices = normaisVertices;
		this.triangulos = triangulos;
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
		this.zMin = zMin;
		this.zMax = zMax;
	}


	

}
