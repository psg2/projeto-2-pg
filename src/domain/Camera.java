package domain;

public class Camera {

	public Ponto C, V, N, U;
	public double d, hx, hy;
	
	public Camera(Ponto C, Ponto V, Ponto N, Ponto U, double d, double hx, double hy) {
		this.C = C;
		this.V = V;
		this.N = N;
		this.U = U;
		this.d = d;
		this.hx = hx;
		this.hy = hy;
	}
	
	public Camera copy(){
		return new Camera(C.copy(), V.copy(), N.copy(), U.copy(), d, hx, hy);
	}
}
